package ru.mcskill.shedar.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryEnderChest;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import ru.mcskill.shedar.Reference;
import ru.mcskill.shedar.Shedar;

public class ItemEnderPouch extends Item {

    protected String unlocalizedName;

    public ItemEnderPouch(String unlocalizedName) {
        this.unlocalizedName = unlocalizedName;

        this.setCreativeTab(Shedar.creativeTab);
        this.setRegistryName(new ResourceLocation(Reference.MODID, unlocalizedName));
        this.setUnlocalizedName(unlocalizedName);
        this.setMaxStackSize(1);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(ItemStack stack, World world, EntityPlayer player, EnumHand hand) {
        InventoryEnderChest inv = player.getInventoryEnderChest();
        if (inv!=null) {
            player.displayGUIChest(inv);
        }
        return ActionResult.newResult(EnumActionResult.PASS, stack);
    }

    public void registerItemModel() {
        Shedar.commonProxy.registerItemRenderer(this, unlocalizedName);
    }
}
