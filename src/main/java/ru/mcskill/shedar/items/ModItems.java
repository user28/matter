package ru.mcskill.shedar.items;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModItems {

    public static ItemMatter matter;
    public static ItemEnderPouch itemEnderPouch;

    public static void init() {
        matter = register(new ItemMatter("matter"));
        itemEnderPouch = register(new ItemEnderPouch("itemEnderPouch"));
    }

    private static <T extends Item> T register(T item) {
        GameRegistry.register(item);

        if (item instanceof ItemBase) {
            ((ItemBase)item).registerItemModel();
        }

        if (item instanceof ItemMatter) {
            ((ItemMatter)item).registerItemModel(item);
        }

        if (item instanceof  ItemEnderPouch) {
            ((ItemEnderPouch)item).registerItemModel();
        }

        return item;
    }
}
