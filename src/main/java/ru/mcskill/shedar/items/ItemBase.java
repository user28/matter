package ru.mcskill.shedar.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import ru.mcskill.shedar.Shedar;

public class ItemBase extends Item {

    protected String name;

    public ItemBase(String name) {
        this.name = name;

        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(Shedar.creativeTab);
    }

    public void registerItemModel() {
        Shedar.commonProxy.registerItemRenderer(this, name);
    }
}
