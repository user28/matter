package ru.mcskill.shedar.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import ru.mcskill.shedar.Reference;
import ru.mcskill.shedar.Shedar;
import ru.mcskill.shedar.handlers.EnumHandler;

import java.util.List;

public class ItemMatter extends Item {

    public ItemMatter(String unrealizedName) {
        this.setUnlocalizedName(unrealizedName);
        this.setRegistryName(new ResourceLocation(Reference.MODID, unrealizedName));
        this.setHasSubtypes(true);
        this.setCreativeTab(Shedar.creativeTab);
    }

    @Override
    public void getSubItems(Item item, CreativeTabs tab, List<ItemStack> items) {
        for (int i = 0; i < EnumHandler.MatterTypes.values().length; i++) {
            items.add(new ItemStack(item, 1, i));
        }
    }

    @Override
    public String getUnlocalizedName(ItemStack item) {
        for (int i = 0; i < EnumHandler.MatterTypes.values().length; i++) {
            if (item.getItemDamage() == i)
                return this.getUnlocalizedName() + "." + EnumHandler.MatterTypes.values()[i].getName();
            else
                continue;
        }
        return this.getUnlocalizedName() + "." + EnumHandler.MatterTypes.BASIC.getName();
    }

    public void registerItemModel(Item item) {
        for (int i = 0; i < EnumHandler.MatterTypes.values().length; i++) {
            Shedar.commonProxy.registerItemRenderer(item, i, "matter_" + EnumHandler.MatterTypes.values()[i].getName());
        }
    }
}
