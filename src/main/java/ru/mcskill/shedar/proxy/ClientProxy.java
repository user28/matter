package ru.mcskill.shedar.proxy;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import ru.mcskill.shedar.Reference;
import ru.mcskill.shedar.Shedar;

public class ClientProxy extends CommonProxy {

    @Override
    public void registerItemRenderer(Item item, String id) {
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(Shedar.modId + ":" + id, "inventory"));
    }

    @Override
    public void registerItemRenderer(Item item, int meta, String fileName) {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(Reference.MODID + ":" + fileName, "inventory"));
    }

    @Override
    public void registerBlockRender(Block block, int meta, String fileName) {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), meta, new ModelResourceLocation(Reference.MODID + ":" + fileName, "inventory"));
    }
}
