package ru.mcskill.shedar;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.mcskill.shedar.blocks.ModBlocks;
import ru.mcskill.shedar.creativeTab.Tab;
import ru.mcskill.shedar.items.ModItems;
import ru.mcskill.shedar.proxy.CommonProxy;

@Mod(modid = Reference.MODID, name = Reference.NAME, version = Reference.VERSION, acceptedMinecraftVersions = "[1.10.2]")
public class Shedar {

    public static final String modId = "Shedar";
    public static final String name = "Shedar";
    public static final String version = "1.0.0";
    public static final Tab creativeTab = new Tab();


    @SidedProxy(serverSide = "ru.mcskill.shedar.proxy.CommonProxy", clientSide = "ru.mcskill.shedar.proxy.ClientProxy")
    public static CommonProxy commonProxy;

    @Mod.Instance(modId)
    public static Shedar instance;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        ModItems.init();
        ModBlocks.init();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }
}