package ru.mcskill.shedar.handlers;

import net.minecraft.util.IStringSerializable;

public class EnumHandler  {

    public enum MatterTypes implements IStringSerializable {
        BASIC("basic", 0),
        ADVANCED ("advanced", 1);

        private int ID;
        private String name;

        MatterTypes(String name, int ID) {
            this.ID = ID;
            this.name = name;
        }


        @Override
        public String getName() {
            return this.name;
        }

        public int getID() {
            return this.ID;
        }

        @Override
        public String toString(){
            return getName();
        }
    }

    public enum FutureTypes implements IStringSerializable {
        BLACKYELLOW("blackyellow", 0),
        BLUE("blue", 1),
        GREYCUB("greycub", 2),
        GREY("grey", 3),
        DARKGREEN("darkgreen", 4),
        BLACK("black", 5),
        BLACKBLUE("blackblue", 6),
        BLACKBLUETWO("blackbluetwo", 7),
        DARKCUBE("darkcube", 8);

        private int ID;
        private String name;

        FutureTypes(String name, int ID) {
            this.ID = ID;
            this.name = name;
        }

        @Override
        public String getName() {
            return this.name;
        }

        public int getID() {
            return this.ID;
        }

        @Override
        public String toString(){
            return getName();
        }
    }

    public enum BoxTypes implements IStringSerializable {
        ONE("one", 0),
        TWO("two", 1);

        private int ID;
        private String name;

        BoxTypes(String name, int ID) {
            this.ID = ID;
            this.name = name;
        }

        @Override
        public String getName() {
            return this.name;
        }

        public int getID() {
            return this.ID;
        }

        @Override
        public String toString(){
            return getName();
        }
    }

    public enum StoneBrick implements IStringSerializable {
        NORMAL("normal", 0),
        MOSSY("mossy", 1),
        CHAPPED("chapped", 2);

        private int ID;
        private String name;

        StoneBrick(String name, int ID) {
            this.ID = ID;
            this.name = name;
        }

        @Override
        public String getName() {
            return this.name;
        }

        public int getID() {
            return this.ID;
        }

        @Override
        public String toString(){
            return getName();
        }
    }

    public enum Wood implements IStringSerializable {
        NORMAL("normal", 0),
        BIRCH("birch", 1),
        JUNGLE("jungle", 2),
        SPRUCE("spruce", 3);

        private int ID;
        private String name;

        Wood(String name, int ID) {
            this.ID = ID;
            this.name = name;
        }

        @Override
        public String getName() {
            return this.name;
        }

        public int getID() {
            return this.ID;
        }

        @Override
        public String toString(){
            return getName();
        }
    }
}
