package ru.mcskill.shedar.creativeTab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import ru.mcskill.shedar.Shedar;
import ru.mcskill.shedar.items.ModItems;

public class Tab extends CreativeTabs{

    public Tab() {
        super(Shedar.modId);
    }

    @Override
    public Item getTabIconItem() {
        return ModItems.matter;
    }
}
