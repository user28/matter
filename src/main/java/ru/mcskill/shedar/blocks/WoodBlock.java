package ru.mcskill.shedar.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import ru.mcskill.shedar.Reference;
import ru.mcskill.shedar.Shedar;
import ru.mcskill.shedar.blocks.item.IMetaBlockName;
import ru.mcskill.shedar.handlers.EnumHandler;

import java.util.List;

public class WoodBlock extends Block implements IMetaBlockName {
    public static final PropertyEnum TYPE = PropertyEnum.create("type", EnumHandler.Wood.class);

    protected String name;

    public WoodBlock(Material material, String unlocalizedName) {
        super(material);

        this.name = unlocalizedName;

        this.setUnlocalizedName(unlocalizedName);
        this.setRegistryName(new ResourceLocation(Reference.MODID, unlocalizedName));
        this.setDefaultState(this.blockState.getBaseState().withProperty(TYPE, EnumHandler.Wood.NORMAL));
        this.setCreativeTab(Shedar.creativeTab);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, TYPE);
    }

    public String getName() {
        return name;
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        EnumHandler.Wood type = (EnumHandler.Wood) state.getValue(TYPE);
        return type.getID();
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(TYPE, EnumHandler.Wood.values()[meta]);
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs creativeTabs, List<ItemStack> items) {
        for (int i = 0; i < EnumHandler.Wood.values().length; i++) items.add(new ItemStack(item, 1, i));
    }

    @Override
    public String getSpecialName(ItemStack stack) {
        return EnumHandler.Wood.values()[stack.getItemDamage()].getName();
    }

    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player) {
        return new ItemStack(Item.getItemFromBlock(this), 1, getMetaFromState(world.getBlockState(pos)));
    }

    @Override
    public int damageDropped(IBlockState state) {
        return getMetaFromState(state);
    }

    public void registerItemModel(Block block) {
        for (int i = 0; i < EnumHandler.Wood.values().length; i++)
            Shedar.commonProxy.registerBlockRender(block, i, getName() + "_" + EnumHandler.Wood.values()[i].getName());
    }
}
