package ru.mcskill.shedar.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import ru.mcskill.shedar.Reference;
import ru.mcskill.shedar.Shedar;
import ru.mcskill.shedar.blocks.item.IMetaBlockName;
import ru.mcskill.shedar.handlers.EnumHandler;

import java.util.List;


public class FutureBlock extends Block implements IMetaBlockName{

    public static final PropertyEnum TYPE = PropertyEnum.create("type", EnumHandler.FutureTypes.class);
    public static final PropertyDirection FACING = PropertyDirection.create("facing");

    protected String name;

    public FutureBlock(Material material, String unlocalizedName) {
        super(material);

        this.name = unlocalizedName;

        this.setUnlocalizedName(unlocalizedName);
        this.setRegistryName(new ResourceLocation(Reference.MODID, unlocalizedName));
        this.setDefaultState(this.blockState.getBaseState().withProperty(TYPE, EnumHandler.FutureTypes.DARKCUBE).withProperty(FACING, EnumFacing.UP));
        this.setCreativeTab(Shedar.creativeTab);
    }

    public String getName() {
        return name;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, TYPE, FACING);
    }

    @Override
    public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        return super.onBlockPlaced(worldIn, pos, BlockPistonBase.getFacingFromEntity(pos, placer), hitX, hitY, hitZ, meta, placer);
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
        worldIn.setBlockState(pos, state.withProperty(FACING, BlockPistonBase.getFacingFromEntity(pos, placer)), 2);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        EnumHandler.FutureTypes type = (EnumHandler.FutureTypes) state.getValue(TYPE);
        return type.getID();
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(TYPE, EnumHandler.FutureTypes.values()[meta]);
    }

    @Override
    public void getSubBlocks(Item item, CreativeTabs creativeTabs, List<ItemStack> items) {
        for (int i = 0; i < EnumHandler.FutureTypes.values().length; i++) {
            items.add(new ItemStack(item, 1, i));
        }
    }

    @Override
    public String getSpecialName(ItemStack stack) {
        return EnumHandler.FutureTypes.values()[stack.getItemDamage()].getName();
    }


    public void registerItemModel(Block block) {
        for (int i = 0; i < EnumHandler.FutureTypes.values().length; i++) {
            Shedar.commonProxy.registerBlockRender(block, i, getName() + "_" + EnumHandler.FutureTypes.values()[i].getName());
        }
    }

}
