package ru.mcskill.shedar.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import ru.mcskill.shedar.Reference;
import ru.mcskill.shedar.Shedar;

public class BaseBlock extends Block {

    protected String name;

    public BaseBlock(Material material, String name) {
        super(material);

        this.name = name;

        this.setCreativeTab(Shedar.creativeTab);
        this.setUnlocalizedName(name);
        this.setRegistryName(new ResourceLocation(Reference.MODID, name));
    }

    public void registerItemModel(ItemBlock itemBlock) {
        Shedar.commonProxy.registerItemRenderer(itemBlock, 0, name);
    }
}
