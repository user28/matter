package ru.mcskill.shedar.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;
import ru.mcskill.shedar.blocks.item.itemModBlock;

public class ModBlocks {

    private static FutureBlock futureBlock = new FutureBlock(Material.IRON, "futureBlock");
    private static BoxBlock boxBlock = new BoxBlock(Material.WOOD, "boxBlock");
    private static BoxBlock planksBlock = new BoxBlock(Material.WOOD, "planks");
    private static StoneBrickBlock stoneBrickBlock = new StoneBrickBlock(Material.ROCK, "brick");
    private static WoodBlock woodBlock = new WoodBlock(Material.WOOD, "wood");
    private static AnimatedFutureBlock animatedFutureBlock = new AnimatedFutureBlock(Material.IRON, "animatedFutureBlock");
    private static BaseBlock asphalt = new BaseBlock(Material.ROCK, "asphalt");
    private static BaseBlock derevo = new BaseBlock(Material.WOOD, "derevo");
    private static BaseBlock metal = new BaseBlock(Material.IRON, "metal");

    public static void init() {
        register(futureBlock, new itemModBlock(futureBlock));
        register(boxBlock, new itemModBlock(boxBlock));
        register(planksBlock, new itemModBlock(planksBlock));
        register(stoneBrickBlock, new itemModBlock(stoneBrickBlock));
        register(woodBlock, new itemModBlock(woodBlock));
        register(animatedFutureBlock, new itemModBlock(animatedFutureBlock));
        register(asphalt);
        register(derevo);
        register(metal);
    }

    private static <T extends Block> T register(T block, ItemBlock itemBlock) {
        GameRegistry.register(block);
        GameRegistry.register(itemBlock);

        if (block instanceof FutureBlock) {
            ((FutureBlock)block).registerItemModel(block);
        }

        if (block instanceof BaseBlock) {
            ((BaseBlock)block).registerItemModel(itemBlock);
        }

        if (block instanceof BoxBlock) {
            ((BoxBlock)block).registerItemModel(block);
        }

        if (block instanceof StoneBrickBlock) {
            ((StoneBrickBlock)block).registerItemModel(block);
        }

        if (block instanceof AnimatedFutureBlock) {
            ((AnimatedFutureBlock)block).registerItemModel(block);
        }

        if (block instanceof WoodBlock) {
            ((WoodBlock)block).registerItemModel(block);
        }

        return block;
    }

    private static <T extends Block> T register(T block) {
        ItemBlock itemBlock = new ItemBlock(block);
        itemBlock.setRegistryName(block.getRegistryName());
        return register(block, itemBlock);
    }
}
