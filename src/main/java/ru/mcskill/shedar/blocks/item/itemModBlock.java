package ru.mcskill.shedar.blocks.item;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class itemModBlock extends ItemBlock {


    public itemModBlock(Block block) {
        super(block);

        this.setHasSubtypes(true);
        this.setMaxDamage(0);
        this.setRegistryName(block.getRegistryName());
    }

    @Override
    public String getUnlocalizedName(ItemStack stack) {
        return super.getUnlocalizedName() + "." + ((IMetaBlockName) this.block).getSpecialName(stack);
    }

    public int getMetadata(int damage) {
        return damage;
    }
}
