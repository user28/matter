package ru.mcskill.shedar.blocks.item;

import net.minecraft.item.ItemStack;

public interface IMetaBlockName {
    String getSpecialName(ItemStack stack);
}
